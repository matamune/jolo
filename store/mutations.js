export default {
  SET_POINT (state, point) {
    const info = state.info
    info.totalPoint = point
    switch (true) {
      case point < 14:
        info.level = 'A1'
        break
      case point >= 14 && point <= 16:
        info.level = 'A2'
        break
      case point >= 17 && point <= 19:
        info.level = 'B1'
        break
      case point >= 20 && point <= 21:
        info.level = 'B2'
        break
      case point >= 22 && point <= 23:
        info.level = 'C1'
        break
      case point >= 24:
        info.level = 'C2'
        break
    }
    state.info = info
  },
  SET_INFO (state, info) {
    state.info = { ...state.info, ...info }
  },
  RESET_POINT (state, point) {
    state.countPoint = point
  }
}
