import Vue from 'vue'
import _pick from 'lodash/pick'

// function qs (params) {
//   return (
//     Object
//       .keys(params)
//       .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
//       .join('&')
//   )
// }

Vue.mixin({
  methods: {
    $_setCookies (cookieName, obj = {}) {
      const info = this.$storage.getCookie(cookieName) || {}
      this.$storage.setCookie(cookieName, { ...info, ...obj })
    },

    async $_setToSheet (key, data, pickProps) {
      const uInfo = this.$storage.getCookie('userInfo') || {}
      if (uInfo.sheetSeted && uInfo.sheetSeted.includes(key)) {
        return
      }
      const listSheet = [
        'https://hooks.zapier.com/hooks/catch/5474362/omph3ro',
        'https://hooks.zapier.com/hooks/catch/5474362/omen6bp',
        'https://hooks.zapier.com/hooks/catch/5474362/omphke2'
      ]
      const info = _pick(data, pickProps)
      try {
        const { data } = await this.$axios.post(listSheet[key], JSON.stringify(info), {
          transformRequest: [function (data, headers) {
            delete headers.common['Content-Type']
            return data
          }]
        })
        if (!uInfo.sheetSeted) {
          uInfo.sheetSeted = [key]
        } else if (!uInfo.sheetSeted.includes(key)) {
          uInfo.sheetSeted.push(key)
        }
        this.$storage.setCookie('userInfo', uInfo)
        return data
      } catch (error) {
        return error
      }
    },

    $_onlyNumber ($event) {
      const keyCode = ($event.keyCode ? $event.keyCode : $event.which)
      if (keyCode < 48 || keyCode > 57) { // 46 is dot
        $event.preventDefault()
      }
    }
  }
})
